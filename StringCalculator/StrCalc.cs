﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringCalculator
{
    public class StrCalc
    {
        const string RegExpressionMatch = @"//[\W]*[\n]";
        
        public int Add(string prmNumbers)
        {
            int returnValue = 0;

            int tempValue = 0;
            if (!Int32.TryParse(prmNumbers, out tempValue))
            {
                tempValue = Calculate(prmNumbers, tempValue);
            }
            returnValue = tempValue;

            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prmNumbers"></param>
        /// <param name="tempValue"></param>
        /// <returns>Aufaddierter Wert</returns>
        private int Calculate(string prmNumbers, int tempValue)
        {
            List<string> splitStrings = new List<string>{
                ",",
                "\n"
            };

            if (prmNumbers.Contains(",\n"))
            {
                throw new Exception("Einem Komma folgt direkt ein Zeilenumbruch, dies ist nicht zulässig");
            }
            else if (FindString(prmNumbers).Length > 0)
            {
                string[] strResults = FindDelimiter(FindString(prmNumbers));
                for (int i = 0; i < strResults.Length; i++)
                {
                    splitStrings.Add(strResults[i]);
                }
            }

            String[] arrString = prmNumbers.Split(splitStrings.ToArray(), StringSplitOptions.RemoveEmptyEntries);

            for (int index = 0; index < arrString.Length; index++)
            {
                int loopValue = 0;
                if (Int32.TryParse(arrString[index], out loopValue))
                {
                    tempValue += this.CheckForNegOrBigNumbers(loopValue);
                }
            }

            return tempValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string[] FindDelimiter(string prmString)
        {
            List<string> arrayDelimiter = new List<string>();

            prmString = FindString(prmString).Remove(0, 2);
            prmString = prmString.Replace("\n", "");

            char lastChar = '\0';
            for (int i = 0; i < prmString.Length; i++ )
            {
                if (lastChar == prmString[i]) 
                {
                    arrayDelimiter[arrayDelimiter.Count - 1] += prmString[i];
                    continue;
                }
                arrayDelimiter.Add(prmString[i].ToString());
                lastChar = prmString[i];
            }

            return arrayDelimiter.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prmNumbers"></param>
        /// <returns></returns>
        private string FindString(string prmNumbers)
        {
            Match match = Regex.Match(prmNumbers, RegExpressionMatch);
            return match.Value;
        }

        /// <summary>
        /// Testen ob negative Zahl eingegeben wurd -> Exception wird geworfen
        /// Testen ob Zahl nicht groesser als 1000 ist
        /// </summary>
        /// <param name="prmNumber"></param>
        /// <returns></returns>
        private int CheckForNegOrBigNumbers(int prmNumber)
        {
            if (prmNumber < 0)
                throw new Exception("Achtung einige Nummern sind negativ");

            if (prmNumber > 1000)
                prmNumber = 0;

            return prmNumber;
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringCalculator.Test
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Test der Add-Methode in der Klasse StrCalc, es wird ein leerer String "" uebergeben, als Ergebnis muss 0
        /// zurueckgegeben werden
        /// </summary>
        [TestMethod]
        public void TestAddMethodeinStrCalcForEmptyString()
        {
            StrCalc strCalc = new StrCalc();

            int result = strCalc.Add("");
            Assert.AreEqual(0, result);
        }

        /// <summary>
        /// Test der Add-Methode in der Klasse StrCalc, es wird eine Ziffer als String uebergeben, als Ergebnis soll
        /// die Ziffer als int zurueckgegeben werden
        /// </summary>
        [TestMethod]
        public void TestAddinStrCalcForOneString()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("2");
            Assert.AreEqual(2, result);
        }

        /// <summary>
        /// Test der Add-Methode in der Klasse StrCalc, es werden mehrer Ziffern als String uebergeben, als Ergebnis soll
        /// die Summe der Ziffer als int zurueckgegeben werden
        /// </summary>
        [TestMethod]
        public void TestAddinStrCalcForManyStrings()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("2,3,4,5,6");
            Assert.AreEqual(2+3+4+5+6, result);
        }

        /// <summary>
        /// Zeilenumbrueche muessen mit betrachtet werden
        /// </summary>
        [TestMethod]
        public void TestAddinStrCalcForLineBreak()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("1\n2,3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

        /// <summary>
        /// wenn User den String ",\n" eingibt soll eine Exception ausgegeben werden
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestAddinStrCalcForLineBreak2()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("1,\n");
        }

        [TestMethod]
        public void Check_Test4a()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("//;\n1;2");
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void CheckNegativeNumbers_Test5()
        {
            StrCalc strCalc = new StrCalc();
            strCalc.Add("1,-1,3");
            strCalc = new StrCalc();
            strCalc.Add("//;\n1;2");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void CheckNegativeNumbers_Test5_2()
        {
            StrCalc strCalc = new StrCalc();
            strCalc.Add("//;\n1;-2");
        }

        [TestMethod]
        public void CheckForBigNumbers_Test5a()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("1,2,1001,3");
            Assert.AreEqual(1 + 2 + 3, result);

        }

        [TestMethod]
        public void CheckForBigNumbers_Test5a_2()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("//;\n1;2;1001;3");
            Assert.AreEqual(1 + 2 + 3, result);

        }

        [TestMethod]
        public void CheckDelimiter_Test5b()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("//***\n1***2***3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

        [TestMethod]
        public void CheckDelimiter_Test5c()
        {
            StrCalc strCalc = new StrCalc();
            int result = strCalc.Add("//*%\n1*2%3");
            Assert.AreEqual(1 + 2 + 3, result);
        }
    }
}
